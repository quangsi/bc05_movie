import React, { useEffect, useState } from "react";
import { movieService } from "../../services/movieService";
import MovieList from "./MovieList/MovieList";
import MovieTabs from "./MovieTabs/MovieTabs";

export default function HomePage() {
  const [movieArr, setMovieArr] = useState([]);
  useEffect(() => {
    movieService
      .getDanhSachPhim()
      .then((res) => {
        setMovieArr(res.data.content);
        //
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  return (
    <div>
      <div className="container mx-auto">
        <MovieList movieArr={movieArr} />
        <MovieTabs />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
      </div>
    </div>
  );
}

// rfc
/**
 * {
    "maPhim": 6377,
    "tenPhim": "Lật Mặt",
    "biDanh": "lat-mat",
    "trailer": "https://www.youtube.com/embed/07zvDW_nj0U",
    "hinhAnh": "https://movienew.cybersoft.edu.vn/hinhanh/lat-mat_gp05.jpg",
    "moTa": "Chuyện phim kể về Hiền (Võ Thành Tâm) - một cựu vận động viên võ thuật sau khi giải nghệ vì chấn thương đã cùng vợ (Ốc Thanh Vân) và con gái về quê thăm gia đình Lâm (Mạc Văn Khoa). Họ bị cuốn vào một cuộc rượt đuổi với tay phản diện A Dìn (Huỳnh Đông). Để bảo vệ cho gia đình nhỏ, Hiền phải đưa vợ con chạy trốn khắp miền Tây sông nước với sự trợ giúp của người bạn thật thà và hài hước.",
    "maNhom": "GP05",
    "ngayKhoiChieu": "2021-04-16T00:00:00",
    "danhGia": 10,
    "hot": null,
    "dangChieu": null,
    "sapChieu": null
}
 */
