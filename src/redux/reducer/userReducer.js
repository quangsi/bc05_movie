import { userLocalService } from "../../services/localStorageService";
import { SET_USER_INFOR } from "../constant/userContant";

const initialState = {
  userInfor: userLocalService.get(),
};

export const userReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_USER_INFOR:
      return { ...state, userInfor: payload };

    default:
      return state;
  }
};

// rxreducer

// admin005 admin0031

// abc123 123456111
